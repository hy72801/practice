use GSSWEB



select L.KEEPER_ID, M.USER_CNAME, M.USER_ENAME, Year(L.LEND_DATE) as BorrowYear, COUNT(Year(L.LEND_DATE)) as BorrowCnt
from dbo.MEMBER_M as M
	inner join dbo.BOOK_LEND_RECORD as L
on M.USER_ID = L.KEEPER_ID
group by L.KEEPER_ID, M.USER_CNAME, M.USER_ENAME,Year(L.LEND_DATE)
order by L.KEEPER_ID, BorrowYear;

