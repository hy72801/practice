use GSSWEB

select TOP(5) L.BOOK_ID as BookId, D.BOOK_NAME as BookName, COUNT(L.BOOK_ID) as QTY
from dbo.BOOK_LEND_RECORD as L
	inner join dbo.BOOK_DATA as D
		on L.BOOK_ID = D.BOOK_ID
group by L.BOOK_ID, D.BOOK_NAME
order by QTY desc